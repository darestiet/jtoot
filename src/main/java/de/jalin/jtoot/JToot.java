package de.jalin.jtoot;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class JToot {
	
	public static final String VISIBILITY_PUBLIC = "public";
	public static final String VISIBILITY_UNLISTED = "unlisted";
	public static final String VISIBILITY_PRIVATE = "private";
	public static final String VISIBILITY_DIRECT = "direct";

	private static final String USER_AGENT = "Mozilla/5.0";
	
	private final String instanceUrl;
	private final String accessToken;

	public JToot(String instanceUrl, String accessToken) {
		this.instanceUrl = instanceUrl;
		this.accessToken = accessToken;
	}

	public int toot(final String visibility, final String status) throws IOException {
		final URL statusesURL = new URL(instanceUrl + "/api/v1/statuses");
		final HttpURLConnection connection = (HttpURLConnection) statusesURL.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("User-Agent", USER_AGENT);
		connection.setRequestProperty ("Authorization", "Bearer " + accessToken);
		connection.setDoOutput(true);
		final OutputStream outputStream = connection.getOutputStream();
		outputStream.write((
			"visibility=" + visibility + "&"
				+ "status=" + status
			).getBytes());
		outputStream.flush();
		outputStream.close();
		int responseCode = connection.getResponseCode();
		return responseCode;
	}

	public static void main(String[] args) {
		final String instanceUrl = args[0];
		final String accessToken = args[1];
		final String visibility = VISIBILITY_DIRECT; 
		final String status = args[2];
		try {
			final JToot jToot = new JToot(instanceUrl, accessToken);
			int responseCode = jToot.toot(visibility, status);
			System.out.println("HTTP Response Code: " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) {
				System.out.println("success");
			} else {
				System.out.println("ERR: something went wrong");
			}
		} catch (IOException e) {
			System.out.println("ERR: " + e.getMessage());
		}
	}


}
